from celery.utils.log import get_task_logger

from mailing.services import Sender
from project.celery import app

logger = get_task_logger('Celery')

sender = Sender()


@app.task(bind=True)
def send_message(self, sms_id, phone, text):
    try:
        sender.send_sms(sms_id, phone, text)
        print(f'Message #{sms_id} sent -- Celery')
        sender.set_status(sms_id, 'Sent')

    except Exception as e:
        print(f'{e.args=}')
        logger.warning(e.args)
