from django.db import models
from django.core.validators import RegexValidator
from django.db.models import Sum, Count


class Mailing(models.Model):
    mailing_start = models.DateTimeField()
    text = models.TextField()
    mailing_end = models.DateTimeField()
    tag_filter = models.CharField(max_length=20, blank=True, null=True)
    operator_filter = models.IntegerField(blank=True, null=True)

    def prepared(self):
        prepared_status = Message.objects.filter(mailing_id_id=self.pk).filter(status='Prepared').aggregate(Count('status'))
        return prepared_status['status__count']

    def sent(self):
        sent_status = Message.objects.filter(mailing_id_id=self.pk).filter(status='Sent').aggregate(Count('status'))
        return sent_status['status__count']


class Client(models.Model):
    phone_regex = RegexValidator(regex=r'^7\d{10}', message="Phone number must be entered in the format: '7xxxxxxxxxx'")
    phone_number = models.CharField(max_length=12, validators=[phone_regex])
    operator_code = models.IntegerField(blank=True, null=True)
    tag = models.CharField(max_length=20)
    timezone = models.IntegerField()

    def save(self, *args, **kwargs):
        self.operator_code = int(self.phone_number[1:4])
        super(Client, self).save(*args, **kwargs)

    def __str__(self):
        return self.phone_number


class Message(models.Model):
    PREPARED = 'Prepared'
    SENT = 'Sent'
    STATUSES = (
        (PREPARED, PREPARED),
        (SENT, SENT)
    )
    send_time = models.DateTimeField()
    status = models.CharField(max_length=50, choices=STATUSES)
    mailing_id = models.ForeignKey(Mailing, on_delete=models.DO_NOTHING)
    client_id = models.ForeignKey(Client, on_delete=models.DO_NOTHING)


