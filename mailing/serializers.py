from rest_framework import serializers
from .models import Mailing, Client, Message
from rest_framework.fields import IntegerField
import datetime
from django.utils import timezone
from project.celery_tasks import send_message


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class MailingListSerializer(serializers.ModelSerializer):
    prepared = IntegerField()
    sent = IntegerField()

    class Meta:
        model = Mailing
        fields = '__all__'


class MailingDetailSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(source='message_set', many=True, read_only=True)

    class Meta:
        model = Mailing
        fields = ['mailing_start', 'text', 'mailing_end', 'tag_filter', 'operator_filter', 'messages']


class MailingCreateSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        mailing = Mailing.objects.create(**validated_data)
        tag_filter = validated_data.get('tag_filter')
        operator_filter = validated_data.get('operator_filter')
        start_time = validated_data['mailing_start']

        end_time = validated_data['mailing_end']
        clients = Client.objects.all()

        if tag_filter != '':
            clients = clients.filter(tag=tag_filter)
        if operator_filter is not None:
            clients = clients.filter(operator_code=operator_filter)
        for client in clients:
            message = Message.objects.create(
                send_time=start_time,
                mailing_id=mailing,
                client_id=client,
                status='Prepared'
            )

            if start_time < timezone.now() < end_time:
                send_message.apply(
                    kwargs={'sms_id': message.pk, 'phone': client.phone_number, 'text': mailing.text},
                )

            elif start_time > datetime.datetime.now():
                send_message.apply_async(
                    kwargs={'sms_id': message.pk, 'phone': client.phone_number, 'text': mailing.text},
                    eta=start_time
                )

        return mailing  # Mailing(**validated_data)

    class Meta:
        model = Mailing
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'
