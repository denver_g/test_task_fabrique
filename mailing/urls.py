
from django.urls import path, include
from .views import (
    MailingListView,
    MailingCreateView,
    MailingUpdateView,
    MailingDeleteView,
    MailingDetailView,
    ClientListView,
    ClientCreateView,
    ClientUpdateView,
    ClientDeleteView,
)

urlpatterns = [
    path('mailing/', MailingListView.as_view()),
    path('mailing/<pk>', MailingDetailView.as_view()),
    path('mailing/create/', MailingCreateView.as_view()),
    path('mailing/update/<pk>', MailingUpdateView.as_view()),
    path('mailing/delete/<pk>', MailingDeleteView.as_view()),
    path('client/', ClientListView.as_view()),
    path('client/create/', ClientCreateView.as_view()),
    path('client/update/<pk>', ClientUpdateView.as_view()),
    path('client/delete/<pk>', ClientDeleteView.as_view()),

]
