import os
from dotenv import load_dotenv
from django_filters.rest_framework import BaseInFilter, CharFilter, FilterSet

import requests

from .models import Mailing, Message

load_dotenv()
SMS_TOKEN = os.getenv('SMS_TOKEN')


class Sender:

    def __init__(self, base_url='https://probe.fbrq.cloud/v1/send/', token=SMS_TOKEN):
        self.base_url = base_url
        self.token = token
        self.headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + self.token
        }

    def send_sms(self, sms_id, phone, text):
        url = self.base_url + str(sms_id)
        data = {
            'id': sms_id,
            'phone': phone,
            'text': text
        }
        resp = requests.post(url=url, headers=self.headers, json=data)
        print(resp.status_code)
        print(resp.json())
        return resp.json()

    def set_status(self, sms_id, status='Sent'):
        message = Message.objects.get(pk=sms_id)
        message.status = status
        message.save()


class InFilter(BaseInFilter, CharFilter):
    pass


class MailingFilter(FilterSet):
    operator = InFilter(field_name='operator_filter', lookup_expr='in')
    tag = InFilter(field_name='tag_filter', lookup_expr='in')

    class Meta:
        model = Mailing
        fields = ['operator_filter', 'tag_filter']



