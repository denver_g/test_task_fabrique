from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    RetrieveAPIView,
)

from .models import Mailing, Client
from .serializers import (
    MailingListSerializer,
    MailingCreateSerializer,
    ClientSerializer,
    MailingDetailSerializer,
)


class MailingListView(ListAPIView):
    serializer_class = MailingListSerializer

    def get_queryset(self):
        queryset = Mailing.objects.all()
        tag = self.request.query_params.get('tag')
        operator = self.request.query_params.get('operator')
        if tag is not None:
            queryset = queryset.filter(tag_filter__iexact=tag)
        if operator is not None:
            queryset = queryset.filter(operator_filter__exact=operator)
        return queryset


class MailingDetailView(RetrieveAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingDetailSerializer


class MailingCreateView(CreateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingCreateSerializer


class MailingUpdateView(UpdateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingCreateSerializer


class MailingDeleteView(DestroyAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingCreateSerializer


class ClientListView(ListAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientCreateView(CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientUpdateView(UpdateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientDeleteView(DestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
